/**
 * Author - Gihan.D Wijerathna
 * Version - 1.0v 
 * Last Modified Date - july 24th 2013
 * 
 * Fully customizable Spinner plugin for loading / Processing events, 
 * easy to fire at any event and there is a way to off the model after 
 * finish target event.Required only jQuery library.
 * 
 * /////////////////////////////////////////////////////
 * Usage - 
 * 
 * Construct SpinnerJs.
 * =====================================================
	$(div).spinnerjs({
	
		width 		: 260,
		height		: 80,
		size		: 30,
		spinner_path: 'images/spinner.gif',
		text		: 'Please wait..'
		
	});
   
   Destruct SpinnerJs
   ======================================================
   $('div').data('spinnerjs').__off();
   
   //////////////////////////////////////////////////
 */

(function($, window, document, undefined){
	
	$.fn.spinnerjs = function(options){
		
		//Defaults options
		var options = $.extend({
				
				width 		: 260,
				height		: 80,
				size		: 30,
				spinner_path: 'images/spinner.gif',
				text		: 'Please wait..'
				
			},options);
		
		var spinner = $(this);
		
		spinner.__off = function(){
			
			$('.spinnerjs-modal-wrapper').fadeOut();;
			
		}
		
		return spinner.each(function(){
			
            $(this).data('spinnerjs', spinner);
			
			var containerWidth  = $(this).width(); // getting Container width
			var containerHeight = $(this).height(); // getting Container Height
			var marginLeft = (containerWidth / 2) - (options.width / 2);
			var marginTop = (containerHeight / 2) - (options.height / 2);
			
			spinner.prepend('<div class="spinnerjs-modal-wrapper"></div>');
			
			$('.spinnerjs-modal-wrapper').css({
				height: containerHeight,
				width: containerWidth
			}).append('<div class="spinnerjs-modal"></div>');
		
			$('.spinnerjs-modal').css({
				height: options.height,
				width: options.width,
				left: marginLeft,
				top: marginTop
			}).append('<div class="spinnerjs-text">'+options.text+'</div><img class="spinnerjs-spinner" src="'+options.spinner_path+'" />')
			
			$('.spinnerjs-spinner').css({
				height: options.size,
				width: options.size,
				left: (options.width / 2) - (options.size / 2),
				top: (options.height / 2) - (options.size / 2),
			}).append('<div class="spinnerjs-modal"></div>');
		})

	}
	
}(jQuery, window, document))
